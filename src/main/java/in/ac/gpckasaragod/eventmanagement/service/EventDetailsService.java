/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.eventmanagement.service;

import in.ac.gpckasaragod.event.management.ui.model.data.EventDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface EventDetailsService {
     public String saveEventDetail(String typeOfEvent, String totalAmount);
     public EventDetails readEventDetail(Integer Id);
      public List<EventDetails> getAllEventDetails();
      public String updateEventDetail(Integer id,String typeOfEvent,String totalAmount);
      public String deleteEventDetail(Integer id);
}
    
