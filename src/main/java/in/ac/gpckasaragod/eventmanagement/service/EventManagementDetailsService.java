/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.eventmanagement.service;

import in.ac.gpckasaragod.event.management.ui.model.data.EventManagementDetails;
import in.ac.gpckasaragod.event.management.ui.model.data.EventManagementUiModel;
import java.util.List;

/**
 *
 * @author student
 */
public interface EventManagementDetailsService {
    public String saveEventManagementDetails(String nameOfCustomer, String contactNo,String address,Integer deptId);
    public EventManagementDetails readEventManagementDetail(Integer Id);
   public List<EventManagementUiModel> getAllEventManagementDetails();
    public String updateEventManagementDetail(Integer id,String nameOfEvent,String contactNo,String address,Integer eventId);
    public String deleteEventManagementDetail(Integer id);

   
}
