/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.event.management.ui.model.data;

/**
 *
 * @author student
 */
public class EventManagementUiModel {
    private Integer Id;
    private String typeOfEvent;
    private Double totalAmount;
    private String nameOfCustomer;
    private String contactNo;
    private String address;
    private Integer eventId;

    public EventManagementUiModel(Integer Id, String typeOfEvent, Double totalAmount, String nameOfCustomer, String contactNo, String address, Integer eventId) {
        this.Id = Id;
        this.typeOfEvent = typeOfEvent;
        this.totalAmount = totalAmount;
        this.nameOfCustomer = nameOfCustomer;
        this.contactNo = contactNo;
        this.address = address;
        this.eventId = eventId;
    }

    

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getTypeOfEvent() {
        return typeOfEvent;
    }

    public void setTypeOfEvent(String typeOfEvent) {
        this.typeOfEvent = typeOfEvent;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNameOfCustomer() {
        return nameOfCustomer;
    }

    public void setNameOfCustomer(String nameOfCustomer) {
        this.nameOfCustomer = nameOfCustomer;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }
    
    
}
