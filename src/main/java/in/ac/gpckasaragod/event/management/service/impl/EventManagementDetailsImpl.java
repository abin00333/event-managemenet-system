package in.ac.gpckasaragod.event.management.service.impl;

import in.ac.gpckasaragod.event.management.ui.model.data.EventManagementDetails;
import in.ac.gpckasaragod.event.management.ui.model.data.EventManagementUiModel;
import in.ac.gpckasaragod.eventmanagement.service.EventManagementDetailsService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author student
 */
public class EventManagementDetailsImpl extends ConnectionServiceImpl implements EventManagementDetailsService {
    @Override
    public String saveEventManagementDetails(String nameOfCustomer, String contactNo,String address,Integer eventId) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO EVENT_MANAGEMENT_DETAILS(NAMEOFCUSTOMER,CONTACTNO,ADDRESS,EVENT_ID) VALUES " 
                    + "('"+nameOfCustomer+ "','"+contactNo+"','" +address+"',"+eventId+")";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(EventDetailsImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
    }

    @Override
   public EventManagementDetails readEventManagementDetail(Integer Id) {     
        EventManagementDetails eventManagementDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM EVENT_MANAGEMENT_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String nameOfCustomer = resultSet.getString("NAMEOFCUSTOMER");
                String contactNo = resultSet.getString("CONTACTNO");
                String address = resultSet.getString("ADDRESS");
                Integer eventId = resultSet.getInt("EVENT_ID");               
                eventManagementDetails = new EventManagementDetails(id,nameOfCustomer,contactNo,address,eventId);
                    
                return eventManagementDetails;
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EventDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         return eventManagementDetails;
       
       
    }
    public List<EventManagementUiModel> getAllEventManagementDetails(){
        List<EventManagementUiModel> eventManagements = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT EVENT_MANAGEMENT_DETAILS.ID,TYPE_OF_EVENT,TOTAL_AMOUNT,NAMEOFCUSTOMER,CONTACTNO,ADDRESS,EVENT_ID FROM EVENT_DETAILS JOIN EVENT_MANAGEMENT_DETAILS ON EVENT_DETAILS.ID=EVENT_ID";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
             String typeOfEvent = resultSet.getString("TYPE_OF_EVENT");
             Double totalAmount = resultSet.getDouble("TOTAL_AMOUNT");
                String nameOfCustomer = resultSet.getString("NAMEOFCUSTOMER");
                String contactNo = resultSet.getString("CONTACTNO");
                String address = resultSet.getString("ADDRESS");
                Integer eventId = resultSet.getInt("EVENT_ID");    
                EventManagementUiModel eventManagementUiModel = new EventManagementUiModel(id, typeOfEvent, totalAmount, nameOfCustomer, contactNo, address, eventId);
                eventManagements.add(eventManagementUiModel);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(EventDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return eventManagements;
    
    }
    @Override
    public String updateEventManagementDetail(Integer Id,String nameOfCustomer,String contactNo,String address,Integer eventId){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE EVENT_MANAGEMENT_DETAILS SET NAMEOFCUSTOMER='"+nameOfCustomer+"',CONTACTNO='"+contactNo+"',ADDRESS='"+address+"',EVENT_ID="+eventId+" WHERE ID="+Id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }
    public String deleteEventManagementDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM EVENT_MANAGEMENT_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }
  

}
