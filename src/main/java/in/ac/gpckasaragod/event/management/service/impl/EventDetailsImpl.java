/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.event.management.service.impl;



import in.ac.gpckasaragod.event.management.ui.model.data.EventDetails;
import in.ac.gpckasaragod.eventmanagement.service.EventDetailsService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class EventDetailsImpl extends ConnectionServiceImpl implements EventDetailsService{

    @Override
    public String saveEventDetail(String typeOfEvent, String totalAmount) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO EVENT_DETAILS (TYPE_OF_EVENT,TOTAL_AMOUNT) VALUES " 
                    + "('"+typeOfEvent+ "','" +totalAmount+"')" ;
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(EventDetailsImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
    }
    @Override
    public EventDetails readEventDetail(Integer Id) {     
        EventDetails eventDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM EVENT_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String typeOfEvent = resultSet.getString("TYPE_OF_EVENT");
                double totalAmount = resultSet.getDouble("TOTAL_AMOUNT");
                eventDetails = new EventDetails(id,typeOfEvent,totalAmount);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EventDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eventDetails;
       
    }
    @Override
    public List<EventDetails> getAllEventDetails(){
        List<EventDetails> events = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM EVENT_DETAILS";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
                String typeOfEvent = resultSet.getString("TYPE_OF_EVENT");
                double totalAmount  = resultSet.getDouble("TOTAL_AMOUNT");
                EventDetails eventDetails = new EventDetails(id,typeOfEvent,totalAmount);
                events.add(eventDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(EventDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return events;
    
    }
    
    @Override
    public String updateEventDetail(Integer id,String typeOfEvent,String totalAmount){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE EVENT_DETAILS SET TYPE_OF_EVENT='"+typeOfEvent+"',TOTAL_AMOUNT='"+totalAmount+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }
    @Override
    public String deleteEventDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM EVENT_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }


    
    
                
}
   

   
    

    



                
                