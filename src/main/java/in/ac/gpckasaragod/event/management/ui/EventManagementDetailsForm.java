/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package in.ac.gpckasaragod.event.management.ui;

import in.ac.gpckasaragod.event.management.service.impl.EventManagementDetailsImpl;
import in.ac.gpckasaragod.event.management.ui.model.data.EventManagementDetails;
import in.ac.gpckasaragod.event.management.ui.model.data.EventManagementUiModel;
import in.ac.gpckasaragod.eventmanagement.service.EventManagementDetailsService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author student
 */
public class EventManagementDetailsForm extends javax.swing.JFrame {
    private EventManagementDetailsService eventManagementDetailsService = new EventManagementDetailsImpl();

    /**
     * Creates new form EventManagementDetailsForm
     */
    public EventManagementDetailsForm() {
        initComponents();
         setLocationRelativeTo(null);
        configureTable();
        loadTableValues();
    }
    
    private void configureTable(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Type of event");
        model.addColumn("Total amount");
        model.addColumn("Id");
        model.addColumn("Name of customer");
        model.addColumn("Contact No");
        model.addColumn("Address");
        tblEventManagementDetails.setModel(model);
        tblEventManagementDetails.getColumn("Id").setWidth(0);
        tblEventManagementDetails.getColumn("Id").setMinWidth(0);
        tblEventManagementDetails.getColumn("Id").setMaxWidth(0);
    }
    
    private void loadTableValues(){
        List<EventManagementUiModel> events = eventManagementDetailsService.getAllEventManagementDetails();
        DefaultTableModel model = (DefaultTableModel) tblEventManagementDetails.getModel();
        model.setRowCount(0);
        for(EventManagementUiModel eventManagementUiModel :events){
            model.addRow(new Object[]{eventManagementUiModel.getTypeOfEvent(),eventManagementUiModel.getTotalAmount(),eventManagementUiModel.getId(),eventManagementUiModel.getNameOfCustomer(),eventManagementUiModel.getContactNo(),eventManagementUiModel.getAddress()});
        }
    }

  

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEventManagementDetails = new javax.swing.JTable();
        btnEdit = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Liberation Sans", 1, 18)); // NOI18N
        jLabel1.setText("EVENT MANAGEMENT DETAILS");

        tblEventManagementDetails.setBackground(new java.awt.Color(0, 204, 204));
        tblEventManagementDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblEventManagementDetails);

        btnEdit.setText("EDIT");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(109, 109, 109))
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(118, 118, 118)
                        .addComponent(btnEdit)
                        .addGap(31, 31, 31)
                        .addComponent(btnDelete)))
                .addContainerGap(35, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(btnAdd)
                    .addContainerGap(405, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete))
                .addGap(28, 28, 28))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(429, Short.MAX_VALUE)
                    .addComponent(btnAdd)
                    .addGap(28, 28, 28)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
JFrame window = new JFrame();
        window.setVisible(true);
        EventManagementDetailForm eventManagementDetailForm = new EventManagementDetailForm();
        window.add(eventManagementDetailForm);
        
        window.setLocationRelativeTo(null);
        window.pack();
        window.addWindowListener(new WindowAdapter(){
            public void winodwClosed(WindowEvent e){
                loadTableValues();
                System.out.print("Window Closed");
            }
            @Override
            public void windowClosing(WindowEvent e){
                super.windowClosing(e);
                System.out.print("Window Closing");
                loadTableValues();

            }
        });        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
int rowIndex = tblEventManagementDetails.convertRowIndexToModel(tblEventManagementDetails.getSelectedRow());
        Integer id = (Integer) tblEventManagementDetails.getValueAt(rowIndex, 2);
        EventManagementDetails eventManagementDetails = eventManagementDetailsService.readEventManagementDetail(id);
        //JOptionPane.showMessageDialog(ComplaintsForm.this, id);
        JFrame window = new JFrame();
        window.setVisible(true);
        EventManagementDetailForm eventManagementDetailForm;
        eventManagementDetailForm = new EventManagementDetailForm(eventManagementDetails);
        window.add(eventManagementDetailForm);
        window.setSize(600, 600);
        window.pack();
        window.setLocationRelativeTo(null);
        window.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e){
                loadTableValues();
                System.out.print("Window Closed");
            }
            @Override
            public void windowClosing(WindowEvent e){
                super.windowClosing(e);
                System.out.print("Window Closing");
                loadTableValues();
            }
        });        // TODO add your handling code here:
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
int rowIndex = tblEventManagementDetails.convertRowIndexToModel(tblEventManagementDetails.getSelectedRow());
        Integer id = (Integer) tblEventManagementDetails.getValueAt(rowIndex, 2);
        int result = JOptionPane.showConfirmDialog(EventManagementDetailsForm.this, "Are you sure to delete");
        if(result == JOptionPane.YES_OPTION){
            String message = eventManagementDetailsService.deleteEventManagementDetail(id);
            JOptionPane.showMessageDialog(EventManagementDetailsForm.this, message);
            loadTableValues();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btnDeleteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EventManagementDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EventManagementDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EventManagementDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EventManagementDetailsForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EventManagementDetailsForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblEventManagementDetails;
    // End of variables declaration//GEN-END:variables
}
