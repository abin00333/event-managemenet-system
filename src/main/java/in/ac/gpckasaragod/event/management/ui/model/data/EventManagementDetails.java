/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.event.management.ui.model.data;

/**
 *
 * @author student
 */
public class EventManagementDetails {
    private Integer id;
    private String nameOfCustomer;
    private String contactNo;
    private String address;
    private Integer eventid;

    public EventManagementDetails(Integer id, String nameOfCustomer, String contactNo, String address, Integer eventid) {
        this.id = id;
        this.nameOfCustomer = nameOfCustomer;
        this.contactNo = contactNo;
        this.address = address;
        this.eventid = eventid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameOfCustomer() {
        return nameOfCustomer;
    }

    public void setNameOfCustomer(String nameOfCustomer) {
        this.nameOfCustomer = nameOfCustomer;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getEventid() {
        return eventid;
    }

    public void setEventid(Integer eventid) {
        this.eventid = eventid;
    }

    @Override
    public String toString() {
        return "EventManagementDetails{" + "id=" + id + ", nameOfCustomer=" + nameOfCustomer + ", contactNo=" + contactNo + ", address=" + address + ", eventid=" + eventid + '}';
    }

   
}
