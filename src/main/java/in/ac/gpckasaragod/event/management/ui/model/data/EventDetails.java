/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.event.management.ui.model.data;

/**
 *
 * @author student
 */
public class EventDetails {
    private Integer Id;
    private String TypeOfEvent;
    private Double TotalAmount;
    public Object getid;

    public EventDetails(Integer Id, String TypeOfEvent, Double TotalAmount) {
        this.Id = Id;
        this.TypeOfEvent = TypeOfEvent;
        this.TotalAmount = TotalAmount;
    }
    @Override
    public String toString(){
        
        return TypeOfEvent+"-"+TotalAmount;
        
    }

    public Integer getId() {
        return Id;
    }

    public String getTypeOfEvent() {
        return TypeOfEvent;
    }

    public Double getTotalAmount() {
        return TotalAmount;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public void setTypeOfEvent(String TypeOfEvent) {
        this.TypeOfEvent = TypeOfEvent;
    }

    public void setTotalAmount(Double TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

}